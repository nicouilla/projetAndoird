package com.example.nikij.programme_projet;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;

public class ModeSoundRecording extends AppCompatActivity {

    private TextView mRecordLabel;
    private TextView mNamefile;
    private Button mRecordBtn;
    private Button mPlayBtn;
    private MediaRecorder mRecorder;
    private MediaPlayer mediaPlayer;


    private String mFileName = null;

    private static final String LOG_TAG ="Record_log";

    private StorageReference mStorage;

    private ProgressDialog mProgress;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_sound_recording);


        mStorage = FirebaseStorage.getInstance().getReference();

        mRecordBtn = findViewById(R.id.button);
        mPlayBtn = findViewById(R.id.buttonPlay);
        mRecordLabel = findViewById(R.id.editText);
        mNamefile = findViewById(R.id.editText2);

        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/son.3gp";

        mProgress = new ProgressDialog(this);

        mRecordBtn.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    startRecording();
                    mRecordLabel.setText("Enregistrement...");
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    stopRecording();
                    mRecordLabel.setText("Enregistrement Terminé");
                }
                return false;
            }

        });

        mPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {

                mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(mFileName);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mediaPlayer.start();
                Toast.makeText(ModeSoundRecording.this, "Le dernier son se joue...",
                        Toast.LENGTH_LONG).show();
            }
        });

    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;

        uploadAudio();

    }

    private void uploadAudio(){

        mProgress.setMessage("Transfert vers FireBase ...");
        mProgress.show();

        String nomFichier = mNamefile.getText().toString();
        nomFichier += ".3gp";

        StorageReference filepath = mStorage.child("Audio").child(nomFichier);
        Uri uri = Uri.fromFile(new File(mFileName));
        filepath.putFile(uri)

                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        mProgress.dismiss();
                        mRecordLabel.setText(R.string.FireBase_ok);

                    }


                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        mProgress.dismiss();
                        mRecordLabel.setText(R.string.FireBase_echec);
                    }
                });
    }

}
