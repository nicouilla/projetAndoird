package com.example.nikij.programme_projet;

import android.content.Context;
import android.graphics.Color;
import android.os.CpuUsageInfo;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.List;

public class CustomButton{

    private String[] colors = {"ROUGE", "BLEU", "VERT", "JAUNE", "BLANC", "NOIR", "VIOLET", "GRIS"};
    private String aName;
    private String aColor;
    private Button aButton;
    private Context aContext;
    private String aSound;

    public CustomButton(Button b, Context c){
        this.aName = "";
        this.aColor = "";
        this.aButton = b;
        this.aContext = c;
        this.aSound = "";
    }

    public String getName() {
        return this.aName;
    }

    public void setName(String pName) {
        this.aName = pName;
        this.aButton.setText(pName);
    }

    public String getSound() {
        return this.aSound;
    }

    public void setSound(String pSound) {
        this.aSound = pSound;
    }

    public String getColor() {
        return this.aColor;
    }

    public void setColor(String pColor) {
        if(this.isColorValid(pColor)){
            this.aColor = pColor;
            this.setButtonColor(pColor);
        }
    }

    public Button getButton() {
        return this.aButton;
    }

    private boolean isColorValid(String pColor){
        for(String s:colors){
            if(s.equals(pColor)){
                return true;
            }
        }
        return false;
    }

    public Context getaContext() {
        return aContext;
    }

    private void setButtonColor(String pColor){
        int c = 0;
        switch (pColor){
            case "ROUGE":
                c = ContextCompat.getColor(aContext, R.color.red);
                break;
            case "BLEU":
                c = ContextCompat.getColor(aContext, R.color.blue);
                break;
            case "VERT":
                c = ContextCompat.getColor(aContext, R.color.green);
                break;
            case "GRIS":
                c = ContextCompat.getColor(aContext, R.color.grey);
                break;
            case "JAUNE":
                c = ContextCompat.getColor(aContext, R.color.yellow);
                break;
            case "BLANC":
                c = ContextCompat.getColor(aContext, R.color.white);
                break;
            case "NOIR":
                c = ContextCompat.getColor(aContext, R.color.black);
                break;
            case "VIOLET":
                c = ContextCompat.getColor(aContext, R.color.violet);
                break;
        }
        if(c != 0) {
            this.aButton.setBackgroundColor(c);

            if (c == ContextCompat.getColor(aContext, R.color.black)) {
                this.aButton.setTextColor(ContextCompat.getColor(aContext, R.color.white));
            } else if (this.aButton.getTextColors().equals(ContextCompat.getColorStateList(aContext, R.color.white))) {
                this.aButton.setTextColor(ContextCompat.getColor(aContext, R.color.black));
            }
        }
    }

    public void activate(){
        if(this.aSound == null || this.aSound.equals("")) {
            this.aButton.setEnabled(false);
        }
    }
}
