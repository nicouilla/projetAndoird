package com.example.nikij.programme_projet;

import android.content.Context;
import android.util.Log;

public class RecSave {

    private String saveName;
    private String[] names;
    private String[] colors;
    private String[] urls;

    public RecSave(String saveName) {
        this.saveName = saveName;
        this.saveName = null;
        this.names = null;
    }

    public RecSave(){
        this.saveName = null;
        this.names = null;
        this.saveName = null;
    }

    public String getSaveName() {
        return this.saveName;
    }

    public void setSaveName(String saveName) {
        this.saveName = saveName;
    }

    public String[] getColors() {
        return this.colors;
    }

    public void setColors(String[] colors) {
        this.colors = colors;
    }

    public String[] getNames() {
        return this.names;
    }

    public void setNames(String[] names) {
        this.names = names;
    }

    public void setUrls(String[] urls){
        this.urls = urls;
    }

    public String[] getUrls() {
        return this.urls;
    }

    public boolean saveREC(Context c){
        if(this.saveName != null && this.names != null && this.colors != null) {
            FileHelper helper = new FileHelper();
            String data = helper.getAssociatedData(this.names, this.colors);
            if(FileHelper.saveToFile(data, this.saveName)){
                Log.e("Bonne nouvelle", "Sauvegarde réussie !");
                return true;
            }else{
                Log.e("Attention", "Sauvegarde ratée...");
                return false;
            }
        }else{
            return false;
        }
    }
}
