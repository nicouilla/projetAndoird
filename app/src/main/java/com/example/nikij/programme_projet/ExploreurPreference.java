package com.example.nikij.programme_projet;

/**
 * Created by wykid on 31/01/2018.
 */
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class ExploreurPreference extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
    }

}
