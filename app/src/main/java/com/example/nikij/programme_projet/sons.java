package com.example.nikij.programme_projet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class sons extends AppCompatActivity {

    private Button bt_mesSons;
    private Button bt_enreg;
    private Button bt_import;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sons);

        bt_mesSons = (Button) findViewById(R.id.sons_button_mes_sons);
        bt_enreg = (Button) findViewById(R.id.sons_button_enregistrer_sons);
        bt_import = (Button) findViewById(R.id.sons_button_importer_son);

        bt_mesSons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(sons.this, AppSounds.class);
                startActivity(intent);*/
                Toast.makeText(sons.this, "Vous pourrez prochainement acceder à une bibliothèque de vos son !", Toast.LENGTH_SHORT).show();
            }
        });

        bt_enreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(sons.this, ModeSoundRecording.class);
                startActivity(intent);
            }
        });

        bt_import.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}
